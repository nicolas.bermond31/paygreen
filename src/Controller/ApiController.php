<?php

namespace App\Controller;

use App\Entity\Transaction;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/create/transaction", name="api_create_transaction")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function createTransaction(): Response
    {
        $em = $this->getDoctrine()->getManager();

        $transaction = new Transaction();
        $number = sha1(random_bytes(10));
        $transaction->setNumber($number);

        $em->persist($transaction);
        $em->flush();

        $lastInsertedTransaction = $em->getRepository(Transaction::class)->findOneBy(['id' => $transaction->getId()]);

        return $this->json([
            'message' => 'Transaction bien créée!',
            'transaction' => $lastInsertedTransaction->getNumber()
        ]);
    }

    /**
     * @Route("/api/users", name="api_users_list")
     * @IsGranted("ROLE_ADMIN")
     */
    public function getUsers(): Response
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository(User::class)->findAll();

        return $this->json([
            'message' => 'Voici la liste des users',
            'users' => $this->get('serializer')->serialize($users, 'json')
        ]);
    }
}
